var express = require('express'),
    fs = require('fs'),
    path = require('path'),
    url = require('url'),
    _ = require('underscore');

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var server = express();

server.all('*', function(req, res) {
    // res.status(404).send('Not found');
    res.json('test');
});

var server_port = process.env.PORT || process.env.VCAP_APP_PORT || 8275;
if (!server_port) {
    console.error("No Port Specified, please run with PORT=<port number> node 'server.js'");
} else {
    server.listen(server_port, function() {
        console.log('Application started on port ' + server_port);
    });
}